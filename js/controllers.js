function MainCtrl($scope) {

    $scope.listQuestions=[];
      $scope.new=false;
    $scope.selected=false;
    $scope.setSelected=function(question,index){
      alert(index);
      $scope.indexQ=index;
      $scope.question=question;
      if(!$scope.selected){
        $scope.selected=true;
      }else {
        $scope.selected=false;
      }
    }
    $scope.addQuestion=function(description,user){

      $scope.selected=false;
      var question={};
      question.reponses=[];
      question.description=description;
      question.user=user;
      question.date=new Date();
      $scope.listQuestions.push(question);
      console.log($scope.listQuestions.length);
      $scope.description=NULL;
      $scope.user=NULL;
    }
    $scope.addReponse=function(reponse,user){
      $scope.new=false;
      var reponse={
        "text" :reponse,
        "date" : new Date(),
        "user" : user
      };
      $scope.listQuestions[$scope.indexQ].reponses.push(reponse);
    }
    $scope.deleteQuestion=function(index){
      $scope.listQuestions.splice(index,1);
        $scope.question={};
        $scope.selected=false;
    }
};


/**
 *
 * Pass all functions into module
 */
angular
    .module('inspinia')
    .controller('MainCtrl', MainCtrl)
